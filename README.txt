Java Property Utility

Copyright (c) Vladimir Dzhuvinov, 2010 - 2012

README

This package provides a Java utility for typed retrieval of java.util.Properties
as boolean, integer, floating point, string and enum values.


Package requirements:

	* Java 1.5+


Package content:

	README.txt                  This file.
	
	LICENSE.txt                 The software license for this package.
	
	property-util-<version>.jar JAR file containing the compiled package
	                            classes.
	
	javadoc/                    The Java docs for this package.
	
	build.xml                   The Apache Ant build file.
	
	src/                        The source code for this package.


[EOF]
